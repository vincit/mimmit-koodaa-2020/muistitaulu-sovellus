import React from 'react';
import PropTypes from 'prop-types';

import PostIt from './PostIt';

const Main = (props) => {
    return (
        <main>
            <div className="notes">
                {props.postIts.map((note, index) => {
                    return (
                        <PostIt
                            id={note.id}
                            key={index}
                            likes={note.likes}
                            onChange={props.onUpdatePost}
                            onDelete={props.onDeletePost}
                            onLike={props.onLikePost}
                            text={note.text}
                        />
                    );
                })}
            </div>
        </main>
    );
};

Main.propTypes = {
    onDeletePost: PropTypes.func.isRequired,
    onLikePost: PropTypes.func.isRequired,
    onUpdatePost: PropTypes.func.isRequired,
    postIts: PropTypes.array.isRequired,
};

export default Main;
