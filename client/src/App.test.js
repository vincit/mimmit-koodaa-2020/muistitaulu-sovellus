import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const buttonElement = getByText(/Add new note/i);
  expect(buttonElement).toBeInTheDocument();
});
