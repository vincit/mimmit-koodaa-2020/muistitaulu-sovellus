# Muistitaulu / Pinboard project

Pinboard application client for the Mimmit koodaa 2020 presentation.

## Pre-install

- Node version: v.10.16.3

## Install the dependencies
  
    npm install

## Running the application

    npm run start

## Testing

    npm run test
